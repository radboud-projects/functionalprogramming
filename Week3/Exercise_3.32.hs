-- Niels Kooij (s1047565)

module Exercise_3_32
where
import Exercise_3_3

data RD = I | V | X | L | C | D | M deriving(Eq, Ord, Show)
data Roman = Roman[RD] deriving(Show)

roman2int :: Roman -> Int
roman2int (Roman y) = sum (map roman2int' (runs y))

roman2int' :: [RD] -> Int
roman2int' [] = 0
roman2int' [x] = roman2int'' x
roman2int' [x,y] = (roman2int'' y) - (roman2int'' x)

roman2int'' :: RD -> Int
roman2int'' I = 1
roman2int'' V = 5
roman2int'' X = 10
roman2int'' L = 50
roman2int'' C = 100
roman2int'' D = 500
roman2int'' M = 1000

int2roman :: Int -> Roman
int2roman n = Roman (int2roman' n)

int2roman' :: Int -> [RD]
int2roman' 0 = []
int2roman' n
  | n >= 1000 = M : int2roman' (n - 1000)
  | n >= 900 = C : M : int2roman' (n - 900)
  | n >= 500 = D : int2roman' (n - 500)
  | n >= 400 = C : D : int2roman' (n - 400)
  | n >= 100 = C : int2roman' (n - 100)
  | n >= 90 = X : C : int2roman' (n - 90)
  | n >= 50 = L : int2roman' (n - 50)
  | n >= 40 = X : L : int2roman' (n - 40)
  | n >= 10 = X : int2roman' (n - 10)
  | n >= 9 = I : X : int2roman' (n - 9)
  | n >= 5 = V : int2roman' (n - 5)
  | n >= 4 = I : V : int2roman' (n - 4)
  | n >= 1 = I : int2roman' (n - 1)
