-- Niels Kooij (s1047565)

module Exercise_3_3
where
import Data.List

runs :: Ord a => [a] -> [[a]]
runs [] = [[]]
runs [x] = [[x]]
runs (x:xs) = d : runs (drop (length d) (x:xs))
  where d = nondecreasing (x:xs)

nondecreasing [] = []
nondecreasing [x] = [x]
nondecreasing (x:x1:xs)
  | x <= x1 = x : nondecreasing (x1:xs)
  | otherwise = [x]
