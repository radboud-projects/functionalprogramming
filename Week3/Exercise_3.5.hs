-- Niels Kooij (s1047565)

module Exercise_3_5
where
import Data.List (sort, permutations, insert)
import Exercise_3_3

type Probes a = [a]
type Property a = a -> Bool

infixr 1 ?->, ?=>

(?->) :: Probes a -> Property b -> Property (a -> b)
(?=>) :: Probes a -> (a -> Property b) -> Property (a -> b)

probes ?-> prop  =  \f -> and [ prop (f x)   | x <- probes ]
probes ?=> prop  =  \f -> and [ prop x (f x) | x <- probes ]

ordered :: (Ord a) => Property [a]
ordered [] = True
ordered [_] = True
ordered (x:x1:xs)
  | x < x1 = ordered (x1:xs)
  | otherwise = False

-- The total amount is n!
permutations :: [a] -> Probes [a]
permutations [] = [[]]
permutations xs = [z | y <- zip [0..] xs, z <- map ((snd y):) (permutations (deleteAt (fst y) xs))]

deleteAt :: Int -> [a] -> [a]
deleteAt n xs = ys ++ (drop 1 zs)
  where (ys, zs) = splitAt n xs

isqrt :: Integer -> Integer
isqrt n = loop 0 3 1
  where loop i k s  | s <= n      = loop (i + 1) (k + 2) (s + k)
                    | otherwise  = i

niftySort :: [a] -> [a]
niftySort _xs  =  []

trustedSort :: (Ord a) => [a] -> [a]
trustedSort  =  sort

-- 3
testRuns :: Bool
testRuns = (permutations ['a'..'d'] ?-> and.map ordered) runs

-- 4
isIntegerSqrt :: Property(Integer -> Integer)
isIntegerSqrt = ([1..1000] ?=> \inp res -> (res^2 <= inp) && ((res+1)^2 > inp))

-- 5
infixr 4  <**>
(<**>) :: Probes a -> Probes b -> Probes (a, b)
xs <**> ys = [(x,y) | x <- xs, y <- ys]
