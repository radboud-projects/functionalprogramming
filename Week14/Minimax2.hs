{-# LANGUAGE TypeFamilies #-}

module Minimax2
where
import Squiggol

data Tree elem  =  Node elem [Tree elem]

-- 2
size, depth :: Tree elem -> Integer
size  = fold size'
  where
  --size' :: TREE elem Integer -> Integer
    size' (NODE _ [])  = 1
    size' (NODE _ res) = 1 + sum res
depth = fold depth'
  where
  --depth' :: TREE elem Integer -> Integer
    depth' (NODE _ [])  = 1
    depth' (NODE _ res) = 1 + maximum res

-- 3
gametree :: (position -> [position]) -> (position -> Tree position)
gametree f = unfold (gametree' f)
  where
  -- gametree' :: (position -> [position]) -> position -> (position -> Tree position)
  -- gametree' f :: position -> (position -> Tree position)
  gametree' f position = NODE position (f position)

-- 4
winning  :: Tree position -> Bool
winning = fold winning'
  where
  --winning' :: TREE elem Bool -> Bool
    winning' (NODE _ []) = False
    winning' (NODE _ xs) = not $ and xs

-- 1
data TREE elem tr = NODE elem [tr]

instance Functor (TREE elem) where
    -- fmap :: (a -> b) -> TREE elem a -> TREE elem b
    fmap f (NODE elem []) = NODE elem []
    fmap f (NODE elem tr) = NODE elem (map f tr)


instance Base (TREE elem) where
    type Rec (TREE elem) = Tree elem

--  inn  ::  TREE elem [Tree elem] -> Tree elem
    inn (NODE elem []) = Node elem []
    inn (NODE elem tr) = Node elem tr

--  out  ::  Tree elem -> TREE elem [Tree elem]
    out (Node elem []) = NODE elem []
    out (Node elem tr) = NODE elem tr
