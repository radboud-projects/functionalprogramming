module Exercise_6_6
where
import Data.List (groupBy, sortBy)
import Data.Either (isLeft)

class Rank key where
  sort  ::  [(key, val)] -> [val]
  rank  ::  [(key, val)] -> [[val]]
  sort  =  concat . rank

genericSort :: (Ord key) => [(key, val)] -> [val]
genericSort kvs  = map snd (sortBy (\ kv1 kv2 -> compare (fst kv1) (fst kv2)) kvs)

instance Rank () where
  sort kvs   =  map snd kvs
  rank kvs   =  [ map snd kvs | not (null kvs) ]

sort' :: (Ord key) => [(key, val)] -> [val]
sort' kvs = map snd (sortBy (\kv1 kv2 -> compare (fst kv1) (fst kv2)) kvs)

-- 1
rank' :: (Ord key) => [(key, val)] -> [[val]]
rank' kvs = map (map snd) (groupBy group (sortBy sort kvs))
  where
    group kv1 kv2 = compare (fst kv1) (fst kv2) == EQ
    sort  kv1 kv2 = compare (fst kv1) (fst kv2)

-- 2 ??
compare' :: Rank key => key -> key -> Ordering
compare' kvs kvt = LT

-- 3
instance (Rank key1, Rank key2) => Rank (key1, key2) where
  rank kvs = map sort (rank kvs')
    where
      kvs' = map (\((x, y), z) -> (x, (y, z))) kvs

-- 4
instance (Rank key1, Rank key2) => Rank (Either key1 key2) where
  rank kvs = rank kvls ++ rank kvrs
    where
      kvls                = [(fromLeft e, val) | (e, val) <- kvs, isLeft e]
      kvrs                = [(fromRight e, val) | (e, val) <- kvs, not $ isLeft e]
      fromLeft (Left x)   = x
      fromRight (Right x) = x

type List elem  =  Either () (elem, [elem])

toList :: [elem] -> List elem
toList []        =  Left ()
toList (a : as)  =  Right (a, as)

-- 5
instance (Rank key) => Rank [key] where
  rank kvs = rank $ map (\(k, v) -> (toList k, v)) kvs

-- 6 ??
repeatedSegments :: (Rank key) => Int -> [key] -> [[Integer]]
repeatedSegments n ks = [[]]

-- instance Rank Base where
