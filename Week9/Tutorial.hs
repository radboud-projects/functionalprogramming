-- f1 = putStr ln x
--  where x = get line

--f2 = do
--  x <- getLine
--  putStrLn x

-- putStr :: String -> IO()
-- getLine :: IOString

-- Do block will process IO actions
--f4 = do
--        x <- getLine
--        y <- getLine
--        putStrLn (x ++ y)


-- return :: a -> IO a


{-

computer thinks of a number
we try to guess this number

readLn :: Read a => IO a

-}

import System.Random

pickANumber :: IO Int
pickANumber = randomRIO (1, 100)

main :: IO ()
main = do
  secret <- pickANumber
  putStrLn "I am thinking of a number between 1 and 100, try to guess it"
  gameLoop secret

feedback :: Int -> Int -> (String, Bool)
feedback secret guess
  | guess < secret = ("Too low", False)
  | guess > secret = ("Too high", False)
  | otherwise = ("That's correct!", True)

gameLoop :: Int -> IO()
gameLoop secret = do
  putStr "guess > "
  guess <- readLn :: IO Int
  let (message, done) = feedback secret guess
  putStrLn message
  if done then
    return ()
  else
    gameLoop secret
