module Mastermind
where
import System.Environment
import System.Random
import Control.Monad
import Data.List

dice :: IO Int
dice = getStdRandom (randomR (1,6))

roll :: IO Int
roll  =  do  a <- dice
             b <- dice
             return (a + b)

-- Pick n random elements from the array
pick :: [a] -> Int -> IO [a]
pick _ 0 = return []
pick l n = do
  i <- randomRIO (0, length l - 1)
  head <- pick l (n - 1)
  return $ (l !! i) : head

colors :: [Char]
colors = "wsgropyb"

-- First argument is the length of the secret
-- Second argument is the amount of turns
-- Third argument is optional and can be used to set the options. The default is colors
main :: IO ()
main = do
  args <- getArgs
  let secretLength = read (args !! 0) :: Int
  let turns = read (args !! 1) :: Int
  let options = if (length args) > 2 then args !! 3 else colors
  secret <- pick colors secretLength
  gameLoop turns secret

gameLoop :: Int -> [Char] -> IO ()
gameLoop 0 _ = putStrLn "Game Over!"
gameLoop n secret = do
  putStrLn ((show n) ++ " turns left")
  putStr "Pick four colors > "
  guess <- readLn
  let (message, done) = feedback secret guess
  putStrLn message
  unless done $ gameLoop (n - 1) secret

feedback :: [Char] -> [Char] -> (String, Bool)
feedback secret guess = (message, done)
  where rightPlace = length $ filter (\(x, y) -> x == y) (zip secret guess)
        done = rightPlace == 4
        rightColor = feedbackRightColor secret guess
        message = if done then
                    "That is the right answer"
                  else
                    "You have " ++ (show rightPlace) ++ " colors on the correct place and " ++ (show rightColor) ++ " colors in the wrong place"

feedbackRightColor :: [Char] -> [Char] -> Int
feedbackRightColor xs ys = feedbackRightColor' (sort as) (sort bs)
  where (as, bs) = unzip [(x, y) | (x, y) <- zip xs ys, x /= y] -- Remove all the correct ones

feedbackRightColor' _ [] = 0
feedbackRightColor' [] _ = 0
feedbackRightColor' (x:xs) (y:ys)
  | x == y = 1 + feedbackRightColor' xs ys
  | x < y = feedbackRightColor' xs (y:ys)
  | x > y = feedbackRightColor' (x:xs) ys
