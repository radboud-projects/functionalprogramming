import Parser
import Control.Applicative

infixl 9 :@

data Lambda var
  =  Var var                   -- variable
  |  Fun var (Lambda var)      -- abstraction/λ-expression
  |  Lambda var :@ Lambda var  -- application
  deriving (Show)

-- lambda, fun, variable, appl :: Parser (Lambda String)
lambda :: Parser (Lambda String)
lambda = appl <|> fun <|> variable <|> char '(' *> lambda <* char ')'

fun :: Parser (Lambda String)
fun =
  do
    char '\\'
    f <- fun'
    return f

fun' :: Parser (Lambda String)
fun' =
  do
    space
    t <- some alphanum
    l <- fun' <|> fun''
    return (Fun t l)

fun'' :: Parser (Lambda String)
fun'' = space *> char '-' *> char '>' *> space *> lambda

variable :: Parser (Lambda String)
variable = Var <$> (space *> some alphanum)

appl :: Parser (Lambda String)
appl =
  do
    space
    char '('
    f <- fun
    char ')'
    space
    l <- lambda
    return (f :@ l)

{-
Funval "x" (Var "x")
ex1 = parse lambda "\\x -> x"

Funval "x" (Funval "y" (Var "x"))
ex2 = parse lambda "\\x y -> x"

Funval "x" (Var "x") :@ Var "x"
ex3 = parse lambda "(\x -> x) x"
-}
