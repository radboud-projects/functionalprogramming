{-# LANGUAGE TypeFamilies #-}

import Prelude hiding (lookup)
import Data.Maybe

toMaybe :: Map () a -> Maybe a
toMaybe Empty      = Nothing
toMaybe (Single v) = Just v

class Key key where
   data Map key :: * -> *

   empty  ::  Map key val
   lookup ::  key -> Map key val -> Maybe val
   insert ::  key -> (Maybe val -> val) -> Map key val -> Map key val

instance Key () where
   data Map () val = Empty | Single val

-- empty  ::  Map () val
   empty = Empty

-- lookup ::  () -> Map () val -> Maybe val
   lookup ()  = toMaybe

   insert () f = Single . f . toMaybe


instance (Key key1, Key key2) => Key (Either key1 key2) where
   data Map (Either key1 key2) val  = EitherMap {
     leftVal  :: Map key1 val,
     rightVal :: Map key2 val
   }

-- empty  ::  (Key key1, Key key2) => Map (Either key1 key2) val
   empty = EitherMap empty empty

-- lookup ::  (Key key1, Key key2) => Either key1 key2 -> Map (Either key1 key2) val -> Maybe val
   lookup (Left key1) (EitherMap l _)  = lookup key1 l
   lookup (Right key2) (EitherMap _ r) = lookup key2 r

-- insert ::  key -> (Maybe val -> val) -> Map (Either key1 key2) val -> Map (Either key1 key2) val
   insert (Left key1) val (EitherMap l r)  = EitherMap (insert key1 val l) r
   insert (Right key2) val (EitherMap l r) = EitherMap l (insert key2 val r)


instance (Key key1, Key key2) => Key (key1, key2) where
   data Map (key1,key2) val  = MapMap {
     mapVal :: Map key1 (Map key2 val)
   }

-- empty  ::  (Key key1, Key key2) => Map (key1, key2) val
   empty = empty

-- lookup ::  (Key key1, Key key2) => (key1, key2) -> Map (key1, key2) val -> Maybe val
   lookup (key1,key2) (MapMap m) = lookup key1 m >>= (lookup key2)

-- insert ::  key -> (Maybe val -> val) -> Map (key1, key2) val -> Map (key1, key2) val
   insert (key1, key2) val (MapMap m) = MapMap (insert key1 (insert' key2 val) m)
     where
       insert' key2' val' = \maybe' -> case maybe' of
                                         Just x  -> insert key2' val' x
                                         Nothing -> insert key2' val' empty

type List elem  =  Either () (elem, [elem])
toList :: [elem] -> List elem
toList []        =  Left ()
toList (a : as)  =  Right (a, as)

instance (Key key) => Key [key] where
   data Map [key] val  = ListMap {
     listVal :: Map (List key) val
   }

   empty = empty

   lookup xs (ListMap m) = lookup (toList xs) m

   insert xs val (ListMap m) = ListMap $ insert (toList xs) val m

data Base = A | T | C | G
   deriving (Show)

type BASE = (Either () (), Either () ())

toBase A = (Left (), Left ())
toBase T = (Left (), Right ())
toBase C = (Right (), Left ())
toBase G = (Right (), Right ())

instance Key Base where
   data Map Base val = B (Map BASE val)

   empty            = B empty

   lookup k (B m)   = lookup (toBase k) m

   insert k f (B m) = B $ insert (toBase k) f m
