-- Niels Kooij (s1047565)

module Char
where
import Data.Char

equal :: String -> String -> Bool
equal s s1 = (map toLower s) == (map toLower s1)

isNumeral :: String -> Bool
isNumeral s = and (map isDigit s)

isBlank :: String -> Bool
isBlank s = and (map isSpace s)

fromDigit :: Char -> Int
fromDigit c = ord c - ord '0'

toDigit :: Int -> Char
toDigit d = chr (d + ord '0')

shift :: Int -> Char -> Char
shift n c
    | isSpace c = c
    | otherwise =
      let number = mod (ord c + n - ord 'A') 26
      in chr (number + ord 'A')

msg  ::  String
msg  =  "MHILY LZA ZBHL XBPZXBL MVYABUHL HWWPBZ JSHBKPBZ \
        \JHLJBZ KPJABT HYJUBT LZA ULBAYVU"

-- map (shift 19) msg
-- FABER EST SUAE QUISQUE FORTUNAE APPIUS CLAUDIUS CAECUS DICTUM ARCNUM EST NEUTRON
