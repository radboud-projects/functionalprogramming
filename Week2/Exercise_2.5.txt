Niels Kooij (s1047565)

(+ 4)               is a function that takes an argument and adds 4
div                 is a function that takes 2 arguments
(div 7) 4           divides 7 by 4 the (div 7) is a function with 1 argument
div (7 4)           Doesn't make sense the first argument (7 4) is not an number
7 `div` 4           is valid. Only if you use the back quotes
+ 3 7               Adds 3 to 7
(+) 3 7             Also works
(b, 'b', "b")       is a tuple of type (Function, Char, String)
(abs, 'abs', "abs") 'abs' is not a valid type
abs ◦ negate        will negate a value and then take the absolute
(* 3) ◦ (+ 3)       will add 3 and then multiply by 3
