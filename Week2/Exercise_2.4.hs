-- Niels Kooij (s1047565)

module Exercise_2_4
where

swap :: (Int, Int) -> (Int, Int)
swap (x, y) = (y, x)

otherFunction :: (Int, Int) -> (Int, Int)
otherFunction (x, y) = (2 * x, 2 * y)

anotherFunction :: (Int, Int) -> (Int, Int)
anotherFunction (x, y) = (x + y, y - x)

-- 2: The original is still valid because the type doesnt matter.
-- The other functions will not be valid because you will do operations on Int

-- 3: (Int, (Char, Bool)) is a tuple of an Int and a tuple of (Char, Bool)
-- (Int, Char, Bool) is a tuple of an Int a Char and a Bool
