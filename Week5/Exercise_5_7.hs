module Exercise_5_7
where

import Prelude hiding (take)
import qualified Data.List as L

unfoldr :: (t -> Maybe (a, t)) -> t -> [a]
unfoldr rep seed = produce seed
  where
    produce seed = case rep seed of
       Just (a, new_seed) -> a : produce new_seed
       Nothing            -> []

apo :: (t -> Either [a] (a, t)) -> t -> [a]
apo rep seed = produce seed
  where
    produce seed = case rep seed of
       Left l     -> l
       Right(a,ns) -> a : produce ns

-- 1
take :: Int -> [a] -> [a]
take n xs = unfoldr (\(n, (x:xs)) -> if n == 0 then Nothing else Just(x, (n - 1, xs))) (n, xs)

-- 2
-- Hacky solution. unfoldr is only used to stitch the list together.
-- filter :: (a -> Bool) -> [a] -> [a]
-- filter p l = unfoldr (\l case l of
--   [] -> Nothing
--   (x:xs) -> Just(x, xs)) [y | y <- l, p l]

-- I wanted to do something like this but I cannot find a way to pass an empty argument. unfoldr needs something to append to the list.
-- filter p l = unfoldr (\l -> case l of
--                               [] -> Nothing
--                               (x:xs) -> if (p x) then Just(x, xs) else Just(_, xs)) l

-- 3
fibs :: [Integer]
fibs = unfoldr (\(n, m) -> Just(n, (m, n + m))) (0, 1)

-- 4
primes :: [Integer]
primes = unfoldr (\(x:xs) -> Just(x, [n | n <- xs, n `mod` x /= 0])) [2..]

-- 5
unfoldr2 :: (t -> Maybe(a, t)) -> t -> [a]
unfoldr2 f x = apo (\x ->
  case f x of
    Nothing -> Left []
    Just y -> Right y) x

-- 6
concat' :: [a] -> [a] -> [a]
concat' xs ys = apo (\xs ->
  case xs of
    [] -> Left ys
    (x:xs) -> Right(x,xs)) xs

-- 7
insert :: (Ord a) => a -> [a] -> [a]
insert x ys = apo (\ys ->
  case ys of
    [] -> Left []
    [y] -> Left[y]
    (y:y1:ys) -> if y <= y1 then Right (y, (y1:ys)) else Right(y1, (y:ys))) (x:ys)
