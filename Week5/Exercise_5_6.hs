module Exercise_5_6
where

data Bit  =  O | I
  deriving (Eq, Ord, Show)

infixr 3 ∧∧
(∧∧) :: Bit -> Bit -> Bit
O ∧∧ _b  =  O
I ∧∧ b   =  b

infixr 2 ||
(||) :: Bit -> Bit -> Bit
O || b   =  b
I || _b  =  I

infixr 4 ><
(><) :: Bit -> Bit -> Bit
O >< O  =  O
O >< I  =  I
I >< O  =  I
I >< I  =  O

mapr :: ((a, state) -> (b, state)) -> (([a], state) -> ([b], state))
mapr f = produce
  where
    produce seed = case fst seed of
      [] -> ([], snd seed)
      (x:xs) -> concat2tuple a (produce (xs, b))
        where (a, b) = f (x, snd seed)

concat2tuple :: a -> ([a], state) -> ([a], state)
concat2tuple x (xs, state) = ((x:xs), state)

type Carry  =  Bit

halfAdder :: (Bit, Bit) -> (Bit, Carry)
halfAdder (a, b) = (a >< b, a ∧∧ b)

fullAdder :: ((Bit, Bit), Carry) -> (Bit, Carry)
fullAdder ((a, b), c) = (a >< b >< c, a ∧∧ c Exercise_5_6.|| a ∧∧ b)

rippleCarryAdder :: (([(Bit, Bit)], Carry) -> ([Bit], Carry))
rippleCarryAdder = mapr fullAdder

test = rippleCarryAdder ([(O,I), (I,I), (I,I), (I,O)], O)
-- We expect: ([I,O,I,O], I)
