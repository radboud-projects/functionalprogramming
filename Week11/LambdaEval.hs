import Data.Maybe
import Control.Monad

infixl 9 :@:

data Expr
  =  Lit Integer      -- a literal
  |  Var String       -- a variable
  |  Bin Expr Op Expr -- binary operator
  |  Abs String Expr  -- a lambda expression
  |  Expr :@: Expr    -- an application
  deriving Show

data Op = Plus | Mult
  deriving Show

data Value = IntVal Integer | FunVal Env String Expr
  deriving Show

type Env = [(String,Value)]

applyIntOp :: Op -> Value -> Value -> Value
applyIntOp op (IntVal v1) (IntVal v2) =
   case op of
      Plus -> IntVal (v1 + v2)
      Mult -> IntVal (v1 * v2)

eval0 :: Expr -> Env -> Value
eval0 (Lit i)        env = IntVal i
eval0 (Var v)        env = fromJust (lookup v env)
eval0 (Bin e1 op e2) env = applyIntOp op (eval0 e1 env) (eval0 e2 env)
eval0 (Abs v b)      env = FunVal env v b
eval0 (ef :@: ea)    env = let FunVal env var body = eval0 ef env
                               arg                 = eval0 ea env
                           in eval0 body ((var,arg):env)

myExpr   = Bin (Lit 12) Plus ((Abs "x" (Bin (Var "x") Mult (Lit 2))) :@: Bin (Lit 4) Plus (Lit 2))
myExpr'  = Bin (Lit 12) Plus ((Abs "x" (Bin (Var "y") Mult (Lit 2))) :@: Bin (Lit 4) Plus (Lit 2))
myExpr'' = Abs "x" (Var "y") :@: (Lit 6)

-- 1
newtype Environment a = EN { fromEN :: Env -> a }

instance Functor Environment where
  -- fmap :: (a -> b) -> Environment a -> Environment b
  fmap f (EN x) = EN $ \env -> f (x env)

instance Applicative Environment where
  -- pure :: a -> Environment a
  pure x = EN $ \env -> x
  -- <*> :: Environment (a -> b) -> Environment a -> Environment b
  (EN f) <*> (EN x) = EN $ f <*> x

instance Monad Environment where
  -- (>>=) :: Environment a -> (a -> Environment b) -> Environment b
  (EN x) >>= f = join $ EN $ pure f <*> x

eval1 :: Expr -> Environment Value
eval1 (Lit i)         = pure (IntVal i)
eval1 (Var v)         = EN $ \env -> fromJust (lookup v env)
eval1 (Bin e1 op e2)  = pure (applyIntOp op) <*> (eval1 e1) <*> (eval1 e2)
eval1 (Abs v b)       = EN $ \env -> FunVal env v b
eval1 (ef :@: ea)     = pure lambda <*> (eval1 ef) <*> (eval1 ea)

lambda :: Value -> Value -> Value
lambda (FunVal env var body) arg = fromEN (eval1 body) ((var,arg):env)

-- 2
eval2' :: Expr -> Env -> Either String Value
eval2' expr env = fromEN (eval2 expr) env

eval2 :: Expr -> Environment (Either String Value)
eval2 (Lit i)        = pure $ Right $ IntVal i
eval2 (Var v)        = EN $ \env -> lookup' v env
eval2 (Bin e1 op e2) = EN $ \env -> pure (applyIntOp op) <*> ((fromEN (eval2 e1) env) >>= guardIsIntVal) <*> ((fromEN (eval2 e2) env) >>= guardIsIntVal)
eval2 (Abs v b)      = EN $ \env -> Right $ (FunVal env v b)
eval2 (ef :@: ea)    = EN $ \env -> join $ pure lambda' <*> ((fromEN (eval2 ef) env) >>= guardIsFunVal) <*> ((fromEN (eval2 ea) env) >>= guardIsIntVal)

lambda' :: Value -> Value -> Either String Value
lambda' (FunVal env var body) arg = fromEN (eval2 body) ((var,arg):env)

guardIsFunVal :: Value -> Either String Value
guardIsFunVal (IntVal _) = Left "First part of lambda must evaluate to FunVal"
guardIsFunVal v          = Right v

guardIsIntVal :: Value -> Either String Value
guardIsIntVal (FunVal _ _ _) = Left "Second part of lambda must evaluate to IntVal"
guardIsIntVal v              = Right v

lookup' :: String -> Env -> Either String Value
lookup' s env =
  case lookup s env of
    Nothing -> Left ("Variable " ++ s ++ " does not exist")
    Just a  -> Right a
