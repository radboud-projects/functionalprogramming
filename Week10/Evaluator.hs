module Evaluator
where

infixl 6 :+:
infixl 7 :*:
infixr 1 :?:

data Expr
  =  Lit Integer    -- a literal
  |  Expr :+: Expr  -- addition
  |  Expr :*: Expr  -- multiplication
  |  Div Expr Expr  -- integer division
  |  Expr :?: Expr  -- non-deterministic choice
  |  Var String     -- a variable

evalA :: (Applicative f) => Expr -> f Integer
evalA (Lit i)      =  pure i
evalA (e1 :+: e2)  =  pure (+)  <*> evalA e1 <*> evalA e2
evalA (e1 :*: e2)  =  pure (*)  <*> evalA e1 <*> evalA e2
evalA (Div e1 e2)  =  pure div  <*> evalA e1 <*> evalA e2

toss  ::  Expr
toss  =  Lit 0 :?: Lit 1

evalN :: Expr -> [Integer]
evalN (Lit l) = [l]
evalN (e1 :+: e2) = [x + y |x <- evalN e1, y <- evalN e2]
evalN (e1 :*: e2) = [x * y |x <- evalN e1, y <- evalN e2]
evalN (Div e1 e2) = [div x y |x <- evalN e1, y <- evalN e2]
evalN (e1 :?: e2) = evalN e1 ++ evalN e2
evalN (Var v) = [0]

-- evalN toss
-- evalN (toss :+: Lit 2 :*: toss)
-- evalN (toss :+: Lit 2 :*: (toss :+: Lit 2 :*: (toss :+: Lit 2 :*: toss)))

newtype Environ a = EN { fromEN :: [(String, Integer)] ->  a }

instance Functor Environ where
-- fmap :: (a -> b) -> Environ a -> Environ b
   fmap f (EN x) = EN $ \env -> f (x env)

instance Applicative Environ where
-- pure a -> Environ a
   pure x = EN $ \env -> x
-- (<*>) :: Environ (a -> b) -> Environ a -> Environ b
   EN f <*> EN x  = EN $ \env -> f env (x env)

instance Monad Environ where
-- return a -> Environ a
   return     = error "return (Monad Environ): not yet implemented"
-- (>>=) :: Environ a -> (a -> Environ a) -> Environ b =
   EN m >>= f = error "<*> (Monad Environ): not yet implemented"

evalR :: Expr -> Environ Integer
evalR (Lit i)      =  pure i
evalR (e1 :+: e2)  =  pure (+)  <*> evalR e1 <*> evalR e2
evalR (e1 :*: e2)  =  pure (*)  <*> evalR e1 <*> evalR e2
evalR (Div e1 e2)  =  pure div  <*> evalR e1 <*> evalR e2
evalR (Var v)      =  EN $ \env -> evalMaybe $ lookup v env

evalMaybe :: Maybe Integer -> Integer
evalMaybe (Just x)   =  x
evalMaybe Nothing    =  0

-- evalR (Var "a" :+: Lit 1) [("a", 4711), ("b", 0815)]
-- evalR (Var "a" :*: Var "b") [("a", 4711), ("b", 0815)]
-- evalR (Var "a" :*: Var "c") [("a", 4711), ("b", 0815)]

newtype EnvND a = EnN { fromEnN :: [(String, Integer)] ->  [a] }

instance Functor EnvND where
-- fmap :: (a -> b) -> EnvND a -> EnvND b
   fmap f (EnN x) = EnN $ \env -> [f y | y <- x env]

instance Applicative EnvND where
--pure a -> EnvND a
  pure x = EnN $ \env -> [x]
--(<*>) :: EnvND (a -> b) -> EnvND a -> EnvND b
  EnN f <*> EnN xs  = EnN $ \env -> [y x | x <- xs env, y <- f env]

evalNR :: Expr -> EnvND Integer
evalNR (Lit i)      =  pure i
evalNR (e1 :+: e2)  =  pure (+)  <*> evalNR e1 <*> evalNR e2
evalNR (e1 :*: e2)  =  pure (*)  <*> evalNR e1 <*> evalNR e2
evalNR (Div e1 e2)  =  pure div  <*> evalNR e1 <*> evalNR e2
evalNR (e1 :?: e2)  =  EnN $ \env -> fromEnN (evalNR e1) env ++ fromEnN (evalNR e2) env
evalNR (Var v)      =  EnN $ \env -> [evalMaybe $ lookup v env]

test = (fromEnN (evalNR (toss :+: Var "a")) [("a", 10), ("b", 5)])
-- [10, 11]
