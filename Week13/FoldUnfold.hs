{-# LANGUAGE LambdaCase  #-}

import Prelude hiding (foldr)

type LIST a = Maybe (a,[a])

out :: [a] -> LIST a
out []      = Nothing
out (x:xs)  = Just (x,xs)

inn :: LIST a -> [a]
inn Nothing       = []
inn (Just (x,xs)) = x:xs

foldr   :: (a -> b -> b) -> b -> ([a] -> b)
foldr f e = consume where
  consume []     = e
  consume (x:xs) = f x (consume xs)

foldRAsfoldr :: (Maybe (a,b) -> b) -> ([a] -> b)
foldRAsfoldr al = foldr helper seed where
  helper a b = al (Just (a,b))
  seed       = al Nothing

-- 1 and 2
foldrAsfoldR :: (a -> b -> b) -> b -> ([a] -> b)
foldrAsfoldR f e = foldR maybeHelper where
   maybeHelper Nothing      = e
   maybeHelper (Just (a,b)) = f a b

foldR   :: (Maybe (a,b) -> b) -> ([a] -> b)
foldR al   = consume where
   consume = al . fmap (fmap consume) . out

unfoldR :: (b -> Maybe (a, b)) -> (b -> [a])
unfoldR co = produce where
   produce = inn . fmap (fmap produce) . co

data LISTB elem res = NIL
                    | CONS elem res
                    deriving (Show)

-- 3
mbP2LB :: Maybe (elem,res) -> LISTB elem res
mbP2LB Nothing            = NIL
mbP2LB (Just (elem, res)) = CONS elem res

lB2MbP :: LISTB elem res -> Maybe(elem,res)
lB2MbP NIL             = Nothing
lB2MbP (CONS elem res) = Just (elem, res)

-- 5
instance Functor (LISTB elem) where
--  fmap :: (a -> b) -> LISTB elem a -> LISTB elem b
    fmap f NIL           = NIL
    fmap f (CONS elem a) = CONS elem (f a)

-- 4
outB :: [a] -> LISTB a [a]
outB []     = NIL
outB (x:xs) = CONS x xs

innB :: LISTB a [a] -> [a]
innB NIL         = []
innB (CONS x xs) = (x:xs)

-- 6
-- foldRB :: (LISTB a b -> b) -> ([a] -> b)
foldRB al   = consume where
   consume = al . fmap consume . outB

-- unfoldRB :: (a -> LISTB b a) -> (b -> [a])
unfoldRB co = consume where
   consume = innB . fmap consume . co

-- emap :: (a -> b) -> LISTB a c -> LISTB b c
emap :: (t -> elem) -> LISTB t res -> LISTB elem res
emap f NIL        = NIL
emap f (CONS a b) = CONS (f a) b

-- 7 and 8
mapAsFold :: (a -> b) -> [a] -> [b]
mapAsFold f = foldRB consume where
    consume NIL        = []
    consume (CONS a b) = f a : b
    
mapAsUnfold f = unfoldRB consume where
    consume []     = NIL
    consume (x:xs) = CONS (f x) xs
