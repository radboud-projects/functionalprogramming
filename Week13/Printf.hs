{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}

module Printf
where

data D  =  D  deriving (Show)
data F  =  F  deriving (Show)
data S  =  S  deriving (Show)

infixr 4 &
(&) :: a -> b -> (a, b)
a & b  =  (a, b)

type family Arg dir res :: *

printf :: (Format dir) => dir -> Arg dir String
printf dir = format dir id ""

class Format dir where
  format :: dir -> (String -> a) -> String -> Arg dir a

type instance Arg D res  =  Int -> res
type instance Arg F res  =  Double -> res
type instance Arg S res  =  String -> res
type instance Arg String res = res
type instance Arg (a, b) res = Arg a res -- Arg a (Arg b res)

instance Format D where
  format D cont out = \i -> cont (out ++ show i)

instance Format F where
  format F cont out = \d -> cont (out ++ show d)

instance Format S where
  format S cont out = \s -> cont (out ++ s)

instance Format String where
  format s cont out = cont (s ++ out)

instance (Format dir1, Format dir2) => Format (dir1, dir2) where
  format (dir1, dir2) cont out = format dir1 cont out

-- printf D 51
-- printf ("I am " & D & " years old.") 51
-- printf ("I am " & D & " " & S & " old.") 1 "year"
-- fmt = "Color " & S & ", Number " & D & ", Float " & F
-- printf fmt "purple" 4711 3.1415
