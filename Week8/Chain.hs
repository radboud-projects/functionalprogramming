module Chain
where

import Satellite
import Tree

-- Costs and dimensions.

type Cost  =  Integer
type Dim   =  (Integer, Integer)

(<**>) :: Dim -> Dim -> With Cost Dim
(i, j) <**> (j', k)
  | j == j'    =  (i * j * k) :- (i, k)
  | otherwise  =  error "<**>: dimensions do not match"

(<***>) :: With Cost Dim -> With Cost Dim -> With Cost Dim
(c1 :- d1) <***> (c2 :- d2)
  =  (c1 + c + c2) :- d
  where c :- d =  d1 <**> d2

-- Minimal costs.

minCost :: [Dim] -> With Cost Dim
minCost [a]  =  0 :- a
minCost as   =  minimum [ minCost bs <***> minCost cs | (bs, cs) <- split as ]

split :: [a] -> [([a], [a])]
split []        =  error "split: empty list"
split [_a]      =  []
split (a : as)  =  ([a], as) : [ (a : bs, cs) | (bs, cs) <- split as]

-- minCost [(10, 30), (30, 5), (5, 60)]
-- minCost [ (i, i + 1) | i <- [1 .. 3] ]
-- minCost [ (i, i + 1) | i <- [1 .. 9] ]

minimumCost :: (size -> size -> With Cost size) -> [size] -> With Cost size
minimumCost f [a] = 0 :- a
minimumCost f as = minimum [starFunction f (minimumCost f bs) (minimumCost f cs) | (bs, cs) <- split as]

starFunction :: (size -> size -> With Cost size) -> With Cost size -> With Cost size -> With Cost size
starFunction f (c1 :- d1) (c2 :- d2)
  = (c1 + c + c2) :- d
  where c :- d = f d1 d2

-- 2 a
-- The Integer inputs are the sizes of the list
costListConcat :: Integer -> Integer -> With Cost Integer
costListConcat a b = a :- a + b

-- 2 b
-- costAddInfinitePrecision

-- 3
optimalChain  :: (size -> size -> With Cost size) -> [size] -> With Cost (With size (Tree size))
optimalChain f [a] = 0 :- (a :- Leaf a)
optimalChain f as = minimum [optimalStarFunction f (optimalChain f bs) (optimalChain f cs) | (bs, cs) <- split as]

optimalStarFunction :: (size -> size -> With Cost size) -> With Cost (With size (Tree size)) -> With Cost (With size (Tree size)) -> With Cost (With size (Tree size))
optimalStarFunction f (c1 :- (d1 :- e1)) (c2 :- (d2 :- e2))
  = (c1 + c + c2) :- (d :- e1 :^: e2)
  where c :- d = f d1 d2

-- 4
-- The concatenation is done from right to left. This way the list grows on the right which is good because the cost of the algorithm depends on the size of the left list.
-- If it was the other way around we would need to add every element in the growing list over and over to the next list.
-- optimalChain (costListConcat) [4, 6, 2, 7, 1]
-- > 19 :- (20 :- Leaf 4 :^: (Leaf 6 :^: (Leaf 2 :^: (Leaf 7 :^: Leaf 1))))

-- 5
