
module Huffman
where
import Satellite
import Tree

import Data.List

-- -------------------------------------------------------------------------------

-- Warm-up: constructing a frequency table.

frequencies  ::  (Ord char) => [char] -> [With Int char]
frequencies xs = sort $ map (\x -> (length x) :- (head x)) (group $ sort xs)

-- -------------------------------------------------------------------------------

-- Constructing a Huffman tree.

huffman :: [With Int char] -> Tree char
huffman xs = huffman' $ map (\(a :- b) -> a :- Leaf b) xs

huffman' [(a :- b)] = b
huffman' (x:x1:xs) = huffman' $ sort ((combine x x1) : xs)
  where combine (a :- b) (a1 :- b1) = (a + a1) :- (b :^: b1)

-- -------------------------------------------------------------------------------

-- Encoding ASCII text.

data Bit = O | I
  deriving (Show, Eq, Ord)

encode :: (Eq char) => Tree char -> [char] -> [Bit]
encode t [] = []
encode t (x:xs) = (encode' ct x) ++ encode t xs
  where ct = codes t

encode' :: (Eq char) => [(char, [Bit])] -> char -> [Bit]
encode' [] c = []
encode' ((x, ys):xs) c
  | x == c = ys
  | otherwise = encode' xs c

codes :: Tree char -> [(char, [Bit])]
codes t = codes' t [O]

codes' :: Tree char -> [Bit] -> [(char, [Bit])]
codes' (Leaf c) xs = [(c, tail $ reverse xs)]
codes' (a :^: b) xs = codes' a (O : xs) ++ codes' b (I : xs)

-- -------------------------------------------------------------------------------

-- Decoding a Huffman binary.

decode :: Tree char -> [Bit] -> [char]
decode t xs = decode' t t xs

decode' :: Tree char -> Tree char -> [Bit] -> [char]
decode' t (Leaf c) [] = [c]
decode' t (Leaf c) (x:xs) = c : decode' t t (x:xs)
decode' t (a :^: b) (x:xs)
  | x == O = decode' t a xs
  | x == I = decode' t b xs

-- -------------------------------------------------------------------------------

-- Some test data.

hw, why :: String
hw =
  "hello world"

-- code = huffman (frequencies hw)
-- encode code hw
-- decode code it
-- decode code it == hw

why =
  "As software becomes more and more complex, it\n\
  \is  more  and  more important to structure it\n\
  \well.  Well-structured  software  is  easy to\n\
  \write,   easy   to   debug,  and  provides  a\n\
  \collection  of modules that can be re-used to\n\
  \reduce future programming costs. Conventional\n\
  \languages place a conceptual limit on the way\n\
  \problems   can   be  modularised.  Functional\n\
  \languages  push  those  limits  back. In this\n\
  \paper we show that two features of functional\n\
  \languages    in    particular,   higher-order\n\
  \functions and lazy evaluation, can contribute\n\
  \greatly  to  modularity.  Since modularity is\n\
  \the key to successful programming, functional\n\
  \languages  are  vitally important to the real\n\
  \world."

-- code = huffman (frequencies why)
-- encode code why
-- decode code it
-- decode code it == why
