module Calculus
where

data Primitive
  =  Sin  -- trigonometric: sine
  |  Cos  -- cosine
  |  Exp  -- exponential
  deriving (Show)

infixl 6 :+:
infixl 7 :*:
infixr 9 :.:

data Function
  =  Const Rational         -- constant function
  |  Id                     -- identity
  |  Prim Primitive         -- primitive function
  |  Function :+: Function  -- addition of functions
  |  Function :*: Function  -- multiplication of functions
  |  Function :.: Function  -- composition of functions
  deriving (Show)

f1 = Const 4
f2 = Id
f3 = Const 2 :*: Id :*: Id :+: Const 5 :*: Id
f4 = Const 2 :*: Prim Sin :+: Id
f5 = Id :*: Prim Cos :.: (Id :*: Id)
f6 = Const 2 :*: (Prim Exp) :.: (Const 2 :*: Id)

-- 1: Apply function
apply :: Function -> (Double -> Double)
apply (Const c) x = (fromRational c)
apply Id x = x
apply (Prim p) x = applyPrimitive p x
apply (f1 :+: f2) x = apply f1 x + apply f2 x
apply (f1 :*: f2) x = apply f1 x * apply f2 x
apply (f1 :.: f2) x = apply f1 (apply f2 x)

applyPrimitive :: Primitive -> (Double -> Double)
applyPrimitive Sin x = sin x
applyPrimitive Cos x = cos x
applyPrimitive Exp x = exp x

-- 2: Derive Function
derive   :: Function -> Function
derive (Const c) = Const 0
derive Id = Const 1
derive (Prim p) = derivePrimitive p
derive (f1 :+: f2) = derive f1 :+: derive f2
derive (f1 :*: f2) = derive f1 :*: f2 :+: f1 :*: derive f2
derive (f1 :.: f2) = derive f1 :.: f2 :*: derive f2

derivePrimitive :: Primitive -> Function
derivePrimitive Sin = (Prim Cos)
derivePrimitive Cos = (Const (negate 1)) :*: (Prim Sin)
derivePrimitive Exp = (Prim Exp)

-- 3: Simplify Function

-- If you apply these rules a few times the Function will be simplified quite a bit.
-- simplify' (simplify' (simplify' (derive f3)))
-- From:
-- ((Const (0 % 1) :*: Id :+: Const (2 % 1) :*: Const (1 % 1)) :*: Id :+: (Const (2 % 1) :*: Id) :*: Const (1 % 1)) :+: (Const (0 % 1) :*: Id :+: Const (5 % 1) :*: Const (1 % 1))
-- To:
-- (Const (2 % 1) :*: Id :+: (Const (2 % 1) :*: Id) :*: Const (1 % 1)) :+: Const (5 % 1)

-- To do it properly I would imagine you have to build some sort of tree and apply an algorithm to it.
-- I have looked into it a bit bit couldn't come up with it so far.
-- Unfortunately I do not have enough time this week to do this properly.

-- Simplification rules
simplify' :: Function -> Function
simplify' (Const c :+: Const c1) = Const (c + c1)
simplify' (Const c :*: Const c1) = Const (c * c1)
simplify' (Const 0 :*: f) = Const 0
simplify' (Const 1 :*: f) = f
simplify' (f1 :+: f2) = simplify' f1 :+: simplify' f2
simplify' (f1 :*: f2) = simplify' f1 :*: simplify' f2
simplify' (f1 :.: f2) = simplify' f1 :.: simplify' f2
simplify' f = f

-- simplify :: Function -> Function
