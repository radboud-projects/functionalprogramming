-- Niels Kooij (s1047565)

module Triangle
where

triangle :: Int -> String
triangle n = triangle' n n

-- total is the total width of the triangle
triangle' :: Int -> Int -> String
triangle' n total
  | n <= 0 = ""
  | otherwise = (triangle' (n - 1) total) ++ (treeRow n total)

-- concats the string n times
stringTimes :: String -> Int -> String
stringTimes s n
  | n <= 0 = ""
  | otherwise = s ++ stringTimes s (n - 1)

-- creates a row with spaces and stars according to n and the total width
treeRow :: Int -> Int -> String
treeRow n total =
  let stars = (2 * n - 1)
      spaces = total - n
  in (stringTimes " " spaces) ++ (stringTimes "*" stars) ++ "\n"

christmasTree :: Int -> String
christmasTree n = christmasTree' n n

christmasTree' :: Int -> Int -> String
christmasTree' n total
  | n <= 0 = ""
  | otherwise = (christmasTree' (n - 1) total) ++ (triangle' n total)

-- To use in ghci prefix the triangle and christmasTree functions with putStr
-- example: 'putStr (triangle 3)' or 'putStr (christmasTree 3)'
